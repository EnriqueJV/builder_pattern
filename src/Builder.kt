fun main( args: Array<String>) {
    Car.Builder()
        .model("Ford")
        .type("Deportivo")
        .color("Rojo").let { println(it) }

    var car = Car.Builder()
        .model("Ford")
        .type("Deportivo")
        .color("Rojo")
        .build()

}


 class Car(
    var model:String?,
    var color: String?,
    var type: String?){
    data class Builder(
        var model:String? = null,
        var color: String? = null,
        var type: String? = null ){

        fun model(model: String) = apply { this.model = model }
        fun color(color: String) = apply { this.color = color }
        fun type( type: String) = apply { this.type = type }
        fun build() = Car(model, color, type)
    }
}



